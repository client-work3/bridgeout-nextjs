/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}'
  ],
  theme: {
    screens: {
      'mb': {'min': '320px', 'max': '639px'},
      // => @media (min-width: 640px and max-width: 767px) { ... }
      
      'sm': '640px',
      // tablet => @media (min-width: 640px) { ... }

      'md': '768px',
      // laptop => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      //my laptop => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    }
  },
  plugins: [require('flowbite/plugin'), require("@tailwindcss/typography")],
}
