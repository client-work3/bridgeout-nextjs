'use client';

import Link from "next/link"
import Image from "next/image";
import { Navbar } from 'flowbite-react';
import Logo from '../media/BridgeOutLogo.png'

export default function Header() {
    return (
        <div className="flex items-stretch h-20 bg-gray-300 shadow-md max-w-full z-40">
            
            <Navbar.Brand className="ml-20 mr-10" as={Link} href="/">
                <Image src={Logo} width={180} height={120} className="" alt="BridgeOut Logo"></Image>
            </Navbar.Brand>
            <Navbar fluid className="bg-gray-300 flex z-50">
                <Navbar.Toggle  />
                <Navbar.Collapse className="bg-gray-300 text-center">
                    <Navbar.Link href="#our-services" className="w-max flex justify-center text-2xl mx-20 text-emerald-500 font-semibold">
                        Our Services
                    </Navbar.Link>
                    <Navbar.Link href="#about-us" className="text-2xl mx-20 text-emerald-500 font-semibold">About Us</Navbar.Link>
                    <Navbar.Link href="#contact-us" className="text-2xl mx-20 text-emerald-500 font-semibold">Contact Us</Navbar.Link>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}