
'use client';

import { Footer } from 'flowbite-react';
import { BsFacebook, BsInstagram, BsTwitter } from 'react-icons/bs';
import Logo from '../media/BridgeOutLogo.png'
import Image from 'next/image';


export default function MainFooter() {
    return (
        <div className="max-w-full">
                <Footer container className='bg-gray-100'>
                    <div className="w-full">
                        <div className="grid w-full justify-between sm:flex sm:justify-between md:flex md:grid-cols-1">
                            <div className='flex justify-center'>
                            <Image src={Logo}  height={100} className="grid justify-center py-5" alt="BridgeOut Logo"></Image>

                            </div>
                            <div className="grid sm:grid-cols-3 sm:pt-5 px-5 gap-8 mb:mt-4 mb:grid-cols-3 mb:gap-6">
                                <div>
                                    <Footer.Title title="About Us" />
                                    <Footer.LinkGroup col>
                                        <Footer.Link href="#our-services">Our Services</Footer.Link>
                                        <Footer.Link href="#testimonials">Testimonials</Footer.Link>
                                    </Footer.LinkGroup>
                                </div>
                                <div>
                                    <Footer.Title title="Ask us" />
                                    <Footer.LinkGroup col>
                                        <Footer.Link href="#contact-us">Contact Us</Footer.Link>
                                        <Footer.Link href="#faq">FAQs</Footer.Link>
                                    </Footer.LinkGroup>
                                </div>
                                <div>
                                    <Footer.Title title="Legal" />
                                    <Footer.LinkGroup col>
                                        <Footer.Link href="#">Privacy Policy</Footer.Link>
                                        <Footer.Link href="#">Terms &amp; Conditions</Footer.Link>
                                    </Footer.LinkGroup>
                                </div>
                            </div>
                        </div>
                        <Footer.Divider />
                        <div className="w-full sm:flex sm:items-center mb:justify-between sm:justify-between">
                            <Footer.Copyright href="#" className="text-center" by="BridgeOut Healthcare Services" year={2023} />
                            <div className="mt-4 flex space-x-6 sm:mt-0 mb:justify-center">
                                <Footer.Icon href="#" icon={BsFacebook} />
                                <Footer.Icon href="#" icon={BsInstagram} />
                                <Footer.Icon href="#" icon={BsTwitter} />
                            </div>
                        </div>
                    </div>
                </Footer>

        </div>

    )
}