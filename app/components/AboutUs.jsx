'use client';
import Image from 'next/image';
import AboutUsImage from '../media/AboutUs.png'

export default function AboutUs() {
    return (
        <div className="max-w-full">
            <div className='bg-gray-200 sm:pb-10 sm:pt-20 px-10 py-20 md:px-20 h-auto'>
                <p className='text-center text-4xl font-semibold  text-gray-500 break-after-column grid justify-items-center'><a id='about-us'>About Us</a></p>
                <div className='gap-8 sm:columns-2 '>
                    <Image src={AboutUsImage} className=' sm:ml-20'></Image>
                    <p className='sm:pt-32 sm:pr-10 text-lg'>At BridgeOut Healthcare Services we understand the importance of maintaining independence and dignity while receiving the care you need. Our team of dedicated and compassionate caregivers is here to support you in the comfort of your own home.</p>
                </div>
            </div>
        </div>

    )
}