'use client';

import { Navbar } from 'flowbite-react';
import { FiPhone } from "react-icons/fi";
import { FaEnvelope, FaRegClock, FaBriefcase } from "react-icons/fa";

export default function Header() {
    return (
        <div className='max-w-full mb:hidden'>
            <Navbar fluid className="bg-emerald-500 flex justify-center shadow">
            <Navbar.Toggle hidden />
            <Navbar.Collapse hidden>
                <Navbar.Link className="flex text-white mx-2 items-center sm:mx-10" href="tel:+447557785103">
                    <FiPhone className='mx-1 ' /> 07557785103
                </Navbar.Link>
                <Navbar.Link className="flex sm:flex-nowrap text-white mx-2 items-center sm:mx-10" href="mailto:angwekfinn@hotmail.com">
                    <FaEnvelope className='mx-1 ' />
                    Email: angwekfinn@hotmail.com
                </Navbar.Link>
                <Navbar.Link className="flex text-white mx-2 items-center sm:mx-10" href="/">
                    <FaRegClock className='mx-1 ' />
                    Opening Times | Mon - Fri | 9am - 5pm
                </Navbar.Link>
                <Navbar.Link className="flex text-white mx-2 items-center sm:mx-10" href='#contact-us'>
                    <FaBriefcase className='mx-1 ' />
                    Careers/Jobs
                </Navbar.Link>
            </Navbar.Collapse>
        </Navbar>
        </div>
        
    )
}