import { FiPhone } from "react-icons/fi";
import Image from 'next/image'
import mainpicture from '../media/main-picture.png'

export default function HomePage() {
    return (
        <div className="max-w-full bg-gray-200">
            <Image src={mainpicture} className=" h-fit  max-w-full" objectFit="cover" 
                sizes="100vw"></Image>
            <div className='md:absolute text-center text-2xl pt-10 md:top-1/4 md:ml-10 md:mt-4 md:text-6xl md:top-1/3 md:ml-20 text-emerald-600 font-bold'>
                <p>Care without compromise</p>
            </div>
            <div className='md:absolute pb-5 md:top-1/3 md:text-4xl md:top-3/4 md:left-3/4 text-emerald-600 font-semibold grid justify-items-center'>
                <p>Looking for care?</p>
                <button
                    type="submit"
                    className="flex items-center rounded-md bg-emerald-600 px-3 py-2 mt-2 text-sm font-semibold text-white shadow-sm hover:bg-emerald-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-emerald-500"
                >
                    <FiPhone className='mr-2' />
                    Call Now
                </button>
            </div>
        </div>

    )
}