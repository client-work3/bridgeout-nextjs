import Image from 'next/image';
import CareImage from '../media/CareService.png'
import CareImage2 from '../media/CareService2.png'
import CareImage3 from '../media/CareService3.png'

export default function OurServices() {
    return (
        <div className="max-w-full">
            <div className='bg-gray-200 md:py-20 py-10 h-auto'>
                <p className='text-center text-4xl font-semibold pb-10 text-gray-500'><a id='our-services'>Our Services</a></p>
                <div className='md:flex justify-center gap-10 px-10'>
                    <div className="group md:w-1/5 pb-5 text-center drop-shadow-xl [perspective:1000px]">
                        <div className="relative h-full w-full shadow-xl transition-all duration-500 [transform-style:preserve-3d] group-hover:[transform:rotateY(180deg)]">
                            <div className=' inset-0'>
                                <Image className='' src={CareImage} ></Image>
                                <p className='absolute top-1/3 pt-4 pl-16 text-2xl font-bold text-white'>Live-in Care</p>
                            </div>
                            <div className="absolute inset-0 h-full w-full  bg-black/100 px-12 text-center text-slate-200 [transform:rotateY(180deg)] [backface-visibility:hidden]">
                                <div className="flex min-h-full flex-col items-center justify-center">
                                    <h1 className="text-3xl font-bold">Live-in Care</h1>
                                    <p className="text-base">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                                    <button className="mt-2 rounded-md text-white bg-emerald-600 py-1 px-2 text-sm hover:bg-emerald-500">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="group md:w-1/5 pb-5 text-center drop-shadow-xl [perspective:1000px]">
                        <div className="relative h-full w-full shadow-xl transition-all duration-500 [transform-style:preserve-3d] group-hover:[transform:rotateY(180deg)]">
                            <div className=' inset-0'>
                                <Image className='' src={CareImage2} ></Image>
                                <p className='absolute top-1/3 pt-4 pl-16 text-2xl font-bold text-white'>Emergency Care</p>
                            </div>
                            <div className="absolute inset-0 h-full w-full  bg-black/100 px-12 text-center text-slate-200 [transform:rotateY(180deg)] [backface-visibility:hidden]">
                                <div className="flex min-h-full flex-col items-center justify-center">
                                    <h1 className="text-3xl font-bold">Emergency Care</h1>
                                    <p className="text-base">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                                    <button className="mt-2 rounded-md text-white bg-emerald-600 py-1 px-2 text-sm hover:bg-emerald-500">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="group md:w-1/5 pb-5 text-center drop-shadow-xl [perspective:1000px]">
                        <div className="relative h-full w-full shadow-xl transition-all duration-500 [transform-style:preserve-3d] group-hover:[transform:rotateY(180deg)]">
                            <div className=' inset-0'>
                                <Image className='' src={CareImage3} ></Image>
                                <p className='absolute top-1/3 pt-4 pl-16 text-2xl font-bold text-white'>Respite Care</p>
                            </div>
                            <div className="absolute inset-0 h-full w-full  bg-black/100 px-12 text-center text-slate-200 [transform:rotateY(180deg)] [backface-visibility:hidden]">
                                <div className="flex min-h-full flex-col items-center justify-center">
                                    <h1 className="text-3xl font-bold">Respite Care</h1>
                                    <p className="text-base">Lorem ipsum dolor sit amet consectetur adipisicing.</p>
                                    <button className="mt-2 rounded-md text-white bg-emerald-600 py-1 px-2 text-sm hover:bg-emerald-500">Read More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}