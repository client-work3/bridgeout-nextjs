'use client';
import { Carousel, Rating } from 'flowbite-react';
import { IoPersonCircle } from "react-icons/io5";


export default function Testimonials() {
    return (
        <div className="max-w-full">
            <div className='bg-white pt-20 pb-4 px-10 md:px-20 h-auto'>
                <p className='text-center text-4xl font-semibold pb-10 text-gray-500'><a id='testimonials'>Testimonials</a></p>
                <div className="md:h-56 h-screen pb-20 sm:h-64 xl:h-80 2xl:h-96">
                    <Carousel>
                        <div className=" md:p-16 px-16 py-8 h-full grid justify-items-center bg-gray-400 dark:bg-gray-700 dark:text-white font-semibold">
                        <IoPersonCircle size={50} />
                            <p>Jane Doe</p>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facere quibusdam voluptatem earum, repellendus sit fugiat iure exercitationem cupiditate aperiam blanditiis sequi rem asperiores deserunt, quisquam quis magni voluptatum cum veniam!</p>
                            <Rating>
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star filled={false} />
                            </Rating>
                        </div>
                        <div className=" md:p-16 px-16 py-8 h-full grid justify-items-center bg-gray-400 dark:bg-gray-700 dark:text-white font-semibold">
                        <IoPersonCircle size={50} />
                            <p>John Doe</p>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facere quibusdam voluptatem earum, repellendus sit fugiat iure exercitationem cupiditate aperiam blanditiis sequi rem asperiores deserunt, quisquam quis magni voluptatum cum veniam!</p>
                            <Rating>
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star filled={false} />
                            </Rating>
                        </div>
                        <div className=" md:p-16 px-16 py-8 h-full grid justify-items-center bg-gray-400 dark:bg-gray-700 dark:text-white font-semibold">
                        <IoPersonCircle size={50} />
                            <p>Mrs Smith</p>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facere quibusdam voluptatem earum, repellendus sit fugiat iure exercitationem cupiditate aperiam blanditiis sequi rem asperiores deserunt, quisquam quis magni voluptatum cum veniam!</p>
                            <Rating>
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star />
                                <Rating.Star filled={false} />
                            </Rating>
                        </div>
                    </Carousel>
                </div>
            </div>
        </div>

    )
}