'use client';

import { Button, Label, TextInput, Select } from 'flowbite-react';
import Link from 'next/link';


export default function Contact() {
    
    return (
        <div className="max-w-full">
            <div className='bg-white md:py-20 pt-20 h-auto'>
                <p className='text-center text-4xl font-semibold pb-10 text-gray-500'><a id='contact-us'>Contact Us</a></p>
                <div className='md:gap-8 md:columns-2 md:px-20 px-10'>
                    <div className='border-2 border-gray-500 break-after-column rounded-xl md:pl-16'>
                        <form className="flex max-w-md flex-col gap-4 m-10 ">
                            <div>
                                <div className="mb-2 block">
                                    <Label value="Name" />
                                </div>
                                <TextInput id="name" type="text" placeholder="Jane Doe" required />
                            </div>
                            <div>
                                <div className="mb-2 block">
                                    <Label value="Type of Enquiry" />
                                </div>
                                <Select id="enquiry" required placeholder='Select a type of enquiry...'>
                                    <option>General Enquiry</option>
                                    <option>Feedback</option>
                                    <option>Question</option>
                                </Select>
                            </div>
                            <div>
                                <div className="mb-2 block">
                                    <Label value="Message" />
                                </div>
                                <TextInput type="text" sizing="lg" />
                            </div>
                            <Button color='success' type="submit">Submit</Button>
                        </form>
                    </div>
                    <div className='grid text-center md:justify-items-center py-24 gap-4 font-semibold text-lg'>
                        <p>Looking for a job?</p>
                        <Button color='success' type="submit" ><a target='_blank' href='https://acrobat.adobe.com/id/urn:aaid:sc:EU:bf193e20-95c0-4cce-9f70-ec57894e1b8a'>Download Application</a></Button>
                        <p>Find the work that matters to you and your community.</p>
                        <Button color='success' type="submit"><Link href="mailto:angwekfinn@hotmail.com" target='_blank'>Upload Application</Link></Button>
                    </div>
                </div>
            </div>
        </div>

    )
}