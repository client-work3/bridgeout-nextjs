import Image from 'next/image';
import Business from '../media/Business.png'

export default function OurServices() {
    return (
        <div className="max-w-full">
            <div className='bg-gray-200 py-20 h-auto'>
                <p className='text-center text-4xl font-semibold pb-10 text-gray-500'>Professional Associations</p>
                <div className='md:flex justify-center md:gap-4 px-10'>
                    <div className='md:w-1/5 pb-5 drop-shadow-xl '>
                        <Image src={Business} ></Image>
                    </div>
                    <div className='md:w-1/5 pb-5 drop-shadow-xl'>
                        <Image src={Business} ></Image>
                    </div>
                    <div className='md:w-1/5 pb-5 drop-shadow-xl'>
                        <Image src={Business} ></Image>
                    </div>
                    <div className='md:w-1/5 pb-5 drop-shadow-xl'>
                        <Image src={Business} ></Image>
                    </div>
                    <div className='md:w-1/5 drop-shadow-xl'>
                        <Image src={Business} ></Image>
                    </div>
                </div>
            </div>
        </div>

    )
}