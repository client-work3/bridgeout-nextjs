'use client';

import TopHeader from './components/TopHeader'
import BottomHeader from './components/BottomHeader'
import HomePage from './components/HomePage';
import OurServices from './components/OurServices';
import Faq from './components/Faq';
import MainFooter from './components/MainFooter';
import AboutUs from './components/AboutUs'
import Testimonials from './components/Testimonials';
import Associations from './components/Associations'
import Contact from './components/Contact';

export default function Home() {
  return (
    <div className="max-w-full bg-gray-200">
      <TopHeader />
      <BottomHeader />
      <HomePage />
      <OurServices />
      <Faq />
      <AboutUs />
      <Testimonials />
      <Associations />
      <Contact />
      <MainFooter />
    </div>

  )
}
